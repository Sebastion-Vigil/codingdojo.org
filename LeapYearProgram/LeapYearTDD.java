import org.junit.Test;


public class JUnitTestCasesForLeapYear(){

@Test
// convention for TDD approach -- methodname_scenario_assertion()
public void isLeapYear_ isDivisibleBy400_returnTrue(){
    LeapYearTDD lyt = new LeapYearTDD();
    Boolean isLeapYear = lyt.isLeapYear(2000);
    Assert.equals(isLeapYear,true);
}

@Test
public void isLeapYear_ isDivisibleBy400_returnFalse(){
    LeapYearTDD lyt = new LeapYearTDD();
    Boolean isLeapYear = lyt.isLeapYear(1900);
    Assert.equals(isLeapYear,false);

}

@Test
public void isLeapYear_isDivisibleBy4ButNotBy100_returnTrue(){
    LeapYearTDD lyt = new LeapYearTDD();
    Boolean isLeapYear = lyt.isLeapYear(2020);
    Assert.equals(isLeapYear,true);
}

@Test
public void isLeapYear_isNotDivisibleBy4IsNotLeapYear_returnFalse(){
    LeapYearTDD lyt = new LeapYearTDD();
    Boolean isLeapYear = lyt.isLeapYear(2021);
    Assert.equals(isLeapYear,false);
}


}
